# Some Corona graphs

These scripts use data published daily by the European CDC to create several plots.

These plots are integrated into a web page that is published via Gitlab Pages at
https://danielkullmann.gitlab.io/corona/.

The plots all show data form the same list of countries, and there are plots for both cases and deaths.

Types of plots that are created:
* Number of accumulated cases and deaths
* Number of accumulated cases and deaths, per 1 million population
* Number of new cases and deaths
* Number of new cases and deaths, per 1 million population

All of these are available as plots showing the raw data, and as plots showing 7-day averages

The point of having plots both of actual numbers and of numbers per 1 million is to compare the two,
and see if one is better than the other. In general, the plot per 1 million is better, because it 
shows the data proportional to the size of the country. However, there is some slight bias in this
type of plot, because larger countries will typically be worse off than smaller countries. I think
that is because scaling the response is not as easy as we think it should be.

Implemented using:
* Python to generate the plots
* Bootstrap and Plain JavaScript for the HTML file, with Chart.js for the graphs

# Ideas for improvement

* Show only two of the main graphs, one for cases, one for deaths, and make the rst of the parameters configurable via dropdown boxes (and maybe shortcuts)
* Show more data, e.g. number of tests
* Show one plot with both number of cases and number of tests
 
