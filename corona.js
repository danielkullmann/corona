/* globals Chart */
"use strict";

var KEY_COUNTRY_CODE = "iso_code";
var KEY_COUNTRY_NAME = "location";
var KEY_CONTINENT = "continent";
var KEY_POPULATION = "population";

var aDataKeys = [
  "hosp_patients",
  "hosp_patients_per_million",
  "icu_patients",
  "icu_patients_per_million",
  "new_cases",
  "new_cases_per_million",
  "new_cases_smoothed",
  "new_cases_smoothed_per_million",
  "new_deaths",
  "new_deaths_per_million",
  "new_deaths_smoothed",
  "new_deaths_smoothed_per_million",
  "new_tests",
  "new_tests_per_thousand",
  "new_tests_smoothed",
  "new_tests_smoothed_per_thousand",
  "positive_rate",
  "reproduction_rate",
  "stringency_index",
  "tests_per_case",
  "tests_units",
  "total_cases",
  "total_cases_per_million",
  "total_deaths",
  "total_deaths_per_million",
  "total_tests",
  "total_tests_per_thousand",
  "total_vaccinations",
  "total_vaccinations_per_hundred",
  "weekly_hosp_admissions",
  "weekly_hosp_admissions_per_million",
  "weekly_icu_admissions",
  "weekly_icu_admissions_per_million"
];

var aCountries = [];
var aAllCountries = [];
var aSelectedKeys = ["total_tests", "total_cases", "total_deaths"];
var oData = null;
var updateHandle = null;
var mCharts = {};
var mHeaders = {};

var mPlotOptions = {
    scales:  {
        xAxes: [{
            type: 'time',
        }]
    },
    plugins:  {
        zoom: {
            zoom: {
                enabled: true,
                drag: true,
                mode: "x"
            }
        },
        colorschemes: {
            scheme: 'brewer.SetOne9'
        }
    }
};

var mPlot2Options = {
    scales:  {
        xAxes: [{
            type: 'time',
        }]
    },
    legend: {
        display: false
    },
    plugins:  {
        zoom: {
            zoom: {
                enabled: true,
                drag: true,
                mode: "x"
            }
        },
        colorschemes: {
            scheme: 'brewer.SetOne9'
        }
    }
};

function dateStringToEpoch(sDate) {
    return new Date(sDate).getTime();
}

function formatDate(oDate) {
    var y = oDate.getFullYear();
    var m = oDate.getMonth() +1;
    var d = oDate.getDate();
    return "" + y + "-" + (m<10 ? "0" : "") + m + "-" + (d < 10 ? "0" : "") + d;
}

function createChart(oPlotElement, sKey, fnGetData) {
    if (!fnGetData) {
        fnGetData = function(oCountryData, date, sKey) {
            return oCountryData[date][sKey];
        };
    }
    var aPlotData = aCountries.map(sCountry => {
        var oCountryData = oData.data[sCountry];
        var xAxis = Object.keys(oCountryData).map(i => parseInt(i, 10)).sort();
        var data = xAxis.map(date => {
            var value = fnGetData(oCountryData, date, sKey);
            if (value !== undefined && ! isNaN(value)) {
                return { x: date, y: value};
            } else {
                return null;
            }
        }).filter(oEntry => oEntry !== null);
        return {
            label: sCountry,
            fill: false,
            pointRadius: 0,
            data: data
        };
    });

    if (mCharts[oPlotElement.id]) {
        mCharts[oPlotElement.id].destroy();
    }

    var oChart = new Chart(oPlotElement, {
        type: 'line',
        data: {
            datasets: aPlotData
        },
        options: mPlotOptions
    });
    mCharts[oPlotElement.id] = oChart;
    oPlotElement.addEventListener("dblclick", function(oEvent) {
        oChart.resetZoom();
    });
}

function createCountryChart() {
    var oPlotElement = document.getElementById("plot-extra-1");
    var oCountrySelect = document.getElementById("select-country");
    var sCountry = oCountrySelect.value;
    if (sCountry === "") return;
    var aPlotData = aDataKeys.map(sKey => {
        var oCountryData = oData.data[sCountry];
        var xAxis = Object.keys(oCountryData).map(i => parseInt(i, 10)).sort();
        var data = xAxis.map(date => {
            var value = oCountryData[date][sKey];
            if (value !== undefined) {
                return { x: date, y: value};
            } else {
                return null;
            }
        }).filter(oEntry => oEntry !== null);
        return {
            label: sKey,
            hidden: aSelectedKeys.indexOf(sKey) < 0,
            fill: false,
            pointRadius: 0,
            data: data
        };
    });

    if (mCharts[oPlotElement.id]) {
        mCharts[oPlotElement.id].destroy();
    }

    var oChart = new Chart(oPlotElement, {
        type: 'line',
        data: {
            datasets: aPlotData
        },
        options: mPlot2Options
    });
    mCharts[oPlotElement.id] = oChart;
    createCustomLegend(oChart);
    oPlotElement.addEventListener("dblclick", function(oEvent) {
        oChart.resetZoom();
    });
}

function updatePlotsForReal() {
    var sGraphType = document.getElementById("select-graph-type").value;
    var aPlots = document.getElementsByClassName("plot");
    Array.from(aPlots).forEach(oPlotElement => {
        var sKey = oPlotElement.id + sGraphType;
        createChart(oPlotElement, sKey);
    });

    redrawCustomCharts();
    makeFunctionPlot("plot-deaths-per-case", function(oCountryData, date, sKey) {
        return oCountryData[date].new_deaths_smoothed / oCountryData[date].new_cases_smoothed;
    });
    makeFunctionPlot("plot-cases-per-death", function(oCountryData, date, sKey) {
        return oCountryData[date].new_cases_smoothed / oCountryData[date].new_deaths_smoothed;
    });
    updateHandle = null;
}

function makeKeyPlot(id, sKey) {
    var oPlotElement = document.getElementById(id);
    createChart(oPlotElement, sKey, function(oCountryData, date, sKey) {
        return oCountryData[date][sKey];
    });
}

function makeFunctionPlot(id, fnGetValue) {
    var oPlotElement = document.getElementById(id);
    createChart(oPlotElement, null, fnGetValue);
}

function updatePlots() {
    if (updateHandle !== null) {
        window.clearTimeout(updateHandle);
    }
    updateHandle = window.setTimeout(updatePlotsForReal, 0);
}

function fillCountryDropDown(dropdownId, fnCountrySelected) {
    var oSelect = document.getElementById(dropdownId);

    // Remove existing options
    while (oSelect.options.length) {
        oSelect.remove(0);
    }

    var oElement = document.createElement("option");
    oElement.value = "";
    oElement.append("--select a country--");
    oSelect.append(oElement);

    for (var i = 0; i < aAllCountries.length; i++ ) {
        var sCountry = aAllCountries[i];
        oElement = document.createElement("option");
        oElement.value = sCountry;
        // TODO Remove this debug line
        if (sCountry === "DEU") {
            oElement.selected = "selected";
        }
        oElement.append(oData.countries[sCountry].name + " (" + sCountry + ")");
        oSelect.append(oElement);
    }
    oSelect.onchange = fnCountrySelected;
    fnCountrySelected();
}

function setVisibilityForDataset(oChart, datasetIndex, visible) {
    var oLegend = document.getElementById("legend-extra-1");
    var oLegendItem = oLegend.getElementsByClassName("legend-"+datasetIndex)[0];
    console.log("setVisibilityForDataset", datasetIndex, oLegendItem, visible);
    if (oLegendItem) {
        var oLabel = oLegendItem.getElementsByClassName("label")[0];
        var meta = oChart.getDatasetMeta(datasetIndex);
        if (visible) {
            oLabel.style = "text-decoration: underline; font-weight: bold;";
            meta.hidden = false;
        } else {
            oLabel.style = "text-decoration: none; font-weight: lighter;";
            meta.hidden = true;
        }
    }
    oChart.update();
}

function toggleVisibilityForDataset(oChart, datasetIndex) {
    setVisibilityForDataset(oChart, datasetIndex, !oChart.isDatasetVisible(datasetIndex));
}

function toggleVisibilityForLegend(oChart, event) {
    var el = event.target;
    while (el.name === null || el.name === undefined) {
        el = el.parentElement;
    }
    var datasetIndex = parseInt(el.name, 10);
    toggleVisibilityForDataset(oChart, datasetIndex);
}

function countKeys(oCountryData) {
    var aDates = Object.keys(oCountryData);
    var mKeyCounts = {};
    for (var i = 0; i < aDates.length; i++) {
        var oItem = oCountryData[aDates[i]];
        var aKeys = Object.keys(oItem);
        for (var j = 0; j < aKeys.length; j++) {
            var sKey = aKeys[j];
            mKeyCounts[sKey] = (mKeyCounts[sKey] || 0) + 1;
        }
    }
    return mKeyCounts;
}

function createCustomLegend(oChart) {
    var i;
    var oLegend = document.getElementById("legend-extra-1");
    var sCountry = document.getElementById("select-country").value;

    while (oLegend.firstChild) {
        oLegend.removeChild(oLegend.firstChild);
    }

    var oCountryData = oData.data[sCountry];
    var mKeyCounts = countKeys(oCountryData);

    var schemePath = mPlotOptions.plugins.colorschemes.scheme.split(".");
    var oScheme = Chart.colorschemes;
    for (var el of schemePath) {
        oScheme = oScheme[el];
    }
    var oTable = document.createElement("table");
    oTable.style = "font-size: x-small;";
    var oTr = document.createElement("tr");
    for (i = 0; i < oChart.data.datasets.length; i++) {
        var oDataset = oChart.data.datasets[i];
        var oTd = document.createElement("td");
        oTd.className = "legend-" + i;
        oTd.name = i;
        var oColorBox = document.createElement("span");
        oColorBox.style = "display: inline-block; vertical-align: middle; margin: 5px 0px; height: 20px; width: 20px; background-color: " + oScheme[i % oScheme.length];
        var oLabel = document.createElement("span");
        oLabel.className = "label";
        oLabel.append(oDataset.label.replaceAll("_", " "));
        oLabel.append(" (" + (mKeyCounts[oDataset.label] || 0) + ")");
        if (oChart.isDatasetVisible(i)) {
            oLabel.style = "text-decoration: underline; font-weight: bold;";
        } else {
            oLabel.style = "text-decoration: none; font-weight: normal;";
        }
        oTd.append(oColorBox);
        oTd.append(" ");
        oTd.append(oLabel);
        oTd.onclick = toggleVisibilityForLegend.bind(null, oChart);
        oTr.append(oTd);
        if (i % 3 == 2 ) {
            oTable.append(oTr);
            oTr = document.createElement("tr");
        }
        oTable.append(oTr);
        oLegend.append(oTable);
    }
}

function hideCountrySelection() {
    var oTextElement = document.getElementById(
        "selected-countries-text"
    );
    var oCheckBoxContainer = document.getElementById(
        "selected-countries"
    );
    if (oTextElement.style.display === "none") {
        oTextElement.style.display = "inline";
        oTextElement.innerText = aCountries.join(", ");
        oCheckBoxContainer.style.display = "none";
    }
}

function toggleCountrySelection() {
    var oTextElement = document.getElementById(
        "selected-countries-text"
    );
    var oCheckBoxContainer = document.getElementById(
        "selected-countries"
    );
    if (oTextElement.style.display === "none") {
        oTextElement.style.display = "inline";
        oTextElement.innerText = aCountries.join(", ");
        oCheckBoxContainer.style.display = "none";
    } else {
        oTextElement.style.display = "none";
        oCheckBoxContainer.style.display = "block";
    }
}

function addCountry(sCountry) {
    var oCountryElement = document.getElementById(sCountry);
    if (oCountryElement) {
        oCountryElement.checked = true;
    }
    aCountries = [];
    var aCountryCheckboxes = document.getElementsByName("country");
    for (var i = 0; i < aCountryCheckboxes.length; i++) {
        var oElement = aCountryCheckboxes.item(i);
        if (oElement.checked) {
            aCountries.push(oElement.id);
        }
    }
    updatePlots();
}

function removeCountry(sCountry) {
    aCountries = aCountries.filter((sValue) => sValue !== sCountry);
    updatePlots();
}

function countryClicked(oEvent) {
    if (oEvent.target.checked) {
        addCountry(oEvent.target.value);
    } else {
        removeCountry(oEvent.target.value);
    }
    window.localStorage.corona = JSON.stringify({
        countries: aCountries
    });
}

function createCountryList(mCountries) {
    var oParent = document.getElementById("selected-countries");
    var oTable = document.createElement("table");
    oTable.style = "font-size: smaller;";
    var oTr = document.createElement("tr");
    var i=0;
    for (var sCountry in mCountries) {
        var sCountryName = " " + mCountries[sCountry].name + " (" + sCountry + ")";
        var oElement = document.createElement("input");
        oElement.type = "checkbox";
        oElement.name = "country";
        oElement.id = sCountry;
        oElement.value = sCountry;
        oElement.onclick = countryClicked;

        var oTd = document.createElement("td");
        oTd.append(oElement);
        oTd.append(sCountryName);
        oTr.append(oTd);
        if (i % 5 == 4 ) {
            oTable.append(oTr);
            oTr = document.createElement("tr");
        }
        oTable.append(oTr);
        oParent.append(oTable);
        i++;
    }
}

function fillCustomDropdown(sSelectId, sPlotId) {
    var oSelect = document.getElementById(sSelectId);

    // Remove existing options
    while (oSelect.options.length) {
        oSelect.remove(0);
    }

    var oElement = document.createElement("option");
    oElement.value = "";
    oElement.append("--select a key--");
    oSelect.append(oElement);

    for (var i = 0; i < aDataKeys.length; i++ ) {
        var sKey = aDataKeys[i];
        oElement = document.createElement("option");
        oElement.value = sKey;
        // TODO Remove this debug line
        oElement.append(sKey);
        oSelect.append(oElement);
    }
    var oPlotElement = document.getElementById(sPlotId);
    oSelect.onchange = function() {
        var sKey = oSelect.value;
        if (sKey) {
            createChart(oPlotElement, sKey);
        }
    };

}

function put(oMap, aKeys, oValue) {
    var oEntry = oMap;
    for (var i = 0; i < aKeys.length-1; i++) {
        var sKey = aKeys[i];
        if (!oEntry[sKey]) {
            oEntry[sKey] = {};
        }
        oEntry = oEntry[sKey];
    }
    oEntry[aKeys[aKeys.length-1]] = oValue;
}

function parseCsv(text) {
    var i;
    var lines = text.split("\n");
    var headers = lines[0].split(",");
    mHeaders = {};
    for (i = 0; i < headers.length; i++) {
        mHeaders[headers[i]] = i;
    }
    oData = {
        data: {},
        countries: {}
    };
    var iMaxDate = 0;
    for (i=1; i< lines.length; i++) {
        var aLine = lines[i].split(",");
        if (aLine.length <= 1) {
            continue;
        }
        var sDate = aLine[mHeaders.date];
        var iDate = dateStringToEpoch(sDate);
        iMaxDate = Math.max(iMaxDate, iDate);
        var sCountry = aLine[mHeaders[KEY_COUNTRY_CODE]];
        var iPopulation = parseInt(aLine[mHeaders[KEY_POPULATION]], 10);
        if (iPopulation < 100000) { continue; }
        if (!oData.countries[sCountry]) {
            oData.countries[sCountry] = {
                name: aLine[mHeaders[KEY_COUNTRY_NAME]],
                population: iPopulation
            };
        }
        for (var j = 0; j < aDataKeys.length; j++) {
            var sKey = aDataKeys[j];
            var fValue = parseFloat(aLine[mHeaders[sKey]]);
            if (!isNaN(fValue)) {
                put(oData.data, [sCountry, iDate, sKey], fValue);
            }
        }
    }
    oData.now = formatDate(new Date(iMaxDate));
    aAllCountries = Array.from(Object.keys(oData.countries));
}

function fillCustomDropdowns() {
    fillCustomDropdown("select-custom-1", "plot-custom-1");
    fillCustomDropdown("select-custom-2", "plot-custom-2");
}

function redrawCustomChart(sSelectId, sPlotId) {
    var oSelect = document.getElementById(sSelectId);
    var oPlotElement = document.getElementById(sPlotId);
    var sKey = oSelect.value;
    if (sKey) {
       createChart(oPlotElement, sKey);
    }
}

function redrawCustomCharts() {
    redrawCustomChart("select-custom-1", "plot-custom-1");
    redrawCustomChart("select-custom-2", "plot-custom-2");
}

function reloadData() {
    var t = new Date().getTime();
    fetch("./owid-covid-data.csv?t="+t).then(oResponse => {
        oResponse.text().then(oResponse => {
            parseCsv(oResponse);
            document.getElementById("now").innerText = oData.now;
            document.getElementById("select-graph-type").onchange = updatePlotsForReal;
            var aAverageWindowTexts = document.getElementsByClassName("average-window");
            for (var i = 0; i < aAverageWindowTexts.length; i++) {
                aAverageWindowTexts.item(i).innerText = oData["average-window"];
            }
            createCountryList(oData.countries);
            if (window.localStorage.corona) {
                var mSavedData = JSON.parse(window.localStorage.corona);
                mSavedData.countries.forEach(sCountry => {
                    addCountry(sCountry);
                });
            } else {
                addCountry("USA");
                addCountry("DEU");
            }
            hideCountrySelection();
            fillCustomDropdowns();
            fillCountryDropDown("select-country", createCountryChart);
        });
    });
}

window.onload = function(event) {
    reloadData();
};
