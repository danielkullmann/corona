.PHONY: owid-covid-data.csv

build: public build-no-data bundle owid-covid-data.csv
	cp owid-covid-data.csv public/
	gzip -9c public/owid-covid-data.csv > public/owid-covid-data.csv.gz

build-no-data: public
	cp index.html public/
	cp corona.js public/
	cp favicon.ico public/

bundle: public
	cat lib/*.js > public/lib.js
	gzip -c -9 public/lib.js > public/lib.js.gz
	cat lib/*.css > public/app.css
	gzip -c -9 public/app.css > public/app.css.gz

public:
	mkdir -p public

owid-covid-data.csv:
	curl --silent -o owid-covid-data.csv https://covid.ourworldindata.org/data/owid-covid-data.csv
